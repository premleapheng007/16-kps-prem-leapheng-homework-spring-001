package com.example.demo.Service;

import com.example.demo.Model.DataTransfer.BookDto;
import com.example.demo.Model.DataTransfer.CategoryDto;
import com.example.demo.Pagination.Pagination;

import java.util.List;

public interface BookService {
//    List<BookDto> findAll();

    BookDto insert(BookDto dto);

    String delete(int id);

    BookDto update(int id, BookDto dto);

    BookDto findOne(int id);

    List<BookDto> getAll(Pagination pagination);

    int countAllBook();

    CategoryDto selectByCateId(int category_id);
//
//    BookDto selectByTitle(String title);
//
//    List<BookDto> selectByCateId(int id, Pagination pagination);


}
