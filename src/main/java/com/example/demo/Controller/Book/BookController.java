package com.example.demo.Controller.Book;

import com.example.demo.Model.BaseApiRespone;
import com.example.demo.Model.DataTransfer.BookDto;
import com.example.demo.Model.Request.BookRequestModel;
import com.example.demo.Model.Respone.BookResponeModel;
import com.example.demo.Pagination.Pagination;
import com.example.demo.Service.BookService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//@RequestMapping("/api")
public class BookController {

    private BookService bookService;

    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

//    @GetMapping("/books")
//    ResponseEntity<BaseApiRespone<List<BookDto>>> findAll(){
//        BaseApiRespone<List<BookDto>> respone=new BaseApiRespone<>();
//        respone.setMessage("Have already find all book");
//        respone.setHttpStatus(HttpStatus.OK);
//        respone.setTimestamp(new Timestamp(System.currentTimeMillis()));
//        respone.setData(bookService.findAll());
//        return new ResponseEntity <>(respone, HttpStatus.OK);
//    }

    @DeleteMapping("/book/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        return new ResponseEntity<>(bookService.delete(id), HttpStatus.OK);
    }

    @PostMapping("/book")
    public ResponseEntity<BaseApiRespone<BookResponeModel>> insert(@RequestBody BookRequestModel requestModel) {
        System.out.println("Request = " + requestModel);
        ModelMapper modelMapper = new ModelMapper();
        BaseApiRespone<BookResponeModel> respone = new BaseApiRespone<>();
        BookDto bookDto = modelMapper.map(requestModel, BookDto.class);
        System.out.println("Test = " + bookDto);
        System.out.println(bookService.insert(bookDto));
        BookResponeModel responeModel = modelMapper.map(requestModel, BookResponeModel.class);
        respone.setMessage("YOU HAVE ADD SUCCESSFULLY!");
        respone.setHttpStatus(HttpStatus.CREATED);
        respone.setTimestamp(new Timestamp(System.currentTimeMillis()));
        respone.setData(responeModel);
        return new ResponseEntity<>(respone, HttpStatus.CREATED);
    }

    @PutMapping("/book/{id}")
    public ResponseEntity<BaseApiRespone<BookResponeModel>> update(@PathVariable int id, @RequestBody BookRequestModel requestModel) {
        ModelMapper modelMapper = new ModelMapper();
        BaseApiRespone<BookResponeModel> respone = new BaseApiRespone<>();
        BookDto dto = modelMapper.map(requestModel, BookDto.class);
        BookResponeModel responeModel = modelMapper.map(bookService.update(id, dto), BookResponeModel.class);
        respone.setMessage("YOU HAVE UPDATED SUCCESSFULLY!");
        respone.setHttpStatus(HttpStatus.OK);
        respone.setData(responeModel);
        respone.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(respone);
    }

    @RequestMapping(value = "/book/{id}", method = RequestMethod.GET)
    public Map<String, Object> findOne(@RequestParam int id) {
        Map<String, Object> result = new HashMap<>();
        BookDto bookDto = bookService.findOne(id);
        if (bookDto == null) {
            result.put("Message", "ID Not Found");
            result.put("Response Code", 404);
        } else {
            bookService.findOne(id);
            result.put("data", bookDto);
            result.put("Message", "Find Data Successfully");
            result.put("Response Code", 200);
        }
        return result;
    }

    @RequestMapping(value = "books", method = RequestMethod.GET)
    public Map<String, Object> getAllData(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                          @RequestParam(value = "limit", required = false, defaultValue = "4") int limit) {
        Pagination pagination = new Pagination(page, limit);
        pagination.setPage(page);
        pagination.setLimit(limit);

        pagination.setTotalCount(bookService.countAllBook());
        pagination.setTotalPages(pagination.getTotalPages());
        Map<String, Object> result = new HashMap<>();
        List<BookDto> bookDtos = bookService.getAll(pagination);
        if (bookDtos.isEmpty()) {
            result.put("Message", "No data in Database");
        } else {
            result.put("Message", "Get Data Successful");
        }
        result.put("Pagination", pagination);
        result.put("Data", bookDtos);
        return result;
    }

//    @RequestMapping(value = "/books", method = RequestMethod.GET, params = "categoryID")
//    public ResponseEntity<BaseApiRespone<List<BookResponeModel>>> selectByCateID(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
//                                                                                 @RequestParam(value = "limit", required = false, defaultValue = "4") int limit,
//                                                                                 @RequestParam("categoryID") int cateID) {
//        ModelMapper mapper = new ModelMapper();
//        Pagination pagination = new Pagination(page, limit);
//        pagination.setPage(page);
//        pagination.setLimit(limit);
//        pagination.nextPage();
//        pagination.previousPage();
//
//        pagination.setTotalCount(bookService.countAllBook());
//        pagination.setTotalPages(pagination.getTotalPages());
//        BaseApiRespone<List<BookResponeModel>> response = new BaseApiRespone<>();
//        List<BookDto> bookDtos = bookService.selectByCateId(cateID, pagination);
//
//        List<BookResponeModel> bookResponses = new ArrayList<>();
//        for (BookDto bookDto : bookDtos) {
//            bookResponses.add(mapper.map(bookDto, BookResponeModel.class));
//        }
//        response.setMessage("Retrieved data by category succesfully");
////        response.setPagination(pagination);
//        response.setData(bookResponses);
//        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
//        response.setHttpStatus(HttpStatus.OK);
//        return ResponseEntity.ok(response);
//
//    }

//    public Map<String, Object> searchBookByTitle(@RequestParam String title){
//        Map<String, Object> result = new HashMap<>();
//        BookDto bookDto = bookService.selectByTitle(title);
//        try{
//            if (bookDto==null){
//                result.put("Message", "Title of Book Not Found");
//                result.put("Response Code", "404");
//            }else{
//                bookService.selectByTitle(title);
//                result.put("Data", bookDto);
//                result.put("Message", "Search Title of Book Successfully");
//                result.put("Response Code", "200");
//            }
//        }catch (Exception e){
//            e.getMessage();
//        }
//        return result;
//    }
}
